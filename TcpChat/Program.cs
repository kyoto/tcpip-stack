﻿using System.Text;
using TcpIpUserspace.Ethernet;
using TcpIpUserspace.Tcp;

var handler = new EthernetHandler();
handler.Listen();

var server = new TcpServer(1337);

var clients = new Dictionary<string, TcpSocket>();
server.ClientConnected += client =>
{
    Console.WriteLine($"Client [{client}] connected.");
    clients[client.ToString()] = client;
    
    client.Send("Your message: "u8.ToArray());
    
    client.MessageReceived += data =>
    {
        client.Send("Your message: "u8.ToArray());
        
        var message = Encoding.Default.GetString(data);
        var broadcastMessage = $"\nMessage from [{client}]: {message}Your message: ";
        var broadcastMessageEncoded = Encoding.Default.GetBytes(broadcastMessage);
        
        Console.Write(broadcastMessage);

        foreach (var other in clients.Values.Where(other => other.ToString() != client.ToString()))
        {
            other.Send(broadcastMessageEncoded);
        }
    };

    client.ClientDisconnected += (ip, port) =>
    {
        clients.Remove(TcpSocket.SocketAddress(ip, port));
        Console.WriteLine($"Client [{client}] disconnected.");
    };
};
