using System.Collections.Concurrent;
using System.Runtime.InteropServices;
using TcpIpUserspace.Tcp;
using Tmds.Linux;

using static Tmds.Linux.LibC;

namespace SocketLibrary;

public static class TcpSocketLibrary
{
    private const int StartDescriptorValue = 4097;

    private static readonly Dictionary<int, TcpSocket?> Sockets = new();
    private static readonly Dictionary<int, BlockingCollection<byte[]>> ReadQueues = new();
    private static int _socketFd = StartDescriptorValue;
    
    [UnmanagedCallersOnly(EntryPoint = "socket")]
    public static int Socket(sa_family_t domain, int type, int protocol)
    {
        if (domain != AF_INET || type != SOCK_STREAM || protocol != IPPROTO_TCP)
        {
            return -1;
        }
        
        Sockets[_socketFd++] = null;
        
        return 0;
    }

    [UnmanagedCallersOnly(EntryPoint = "connect")]
    public static unsafe int Connect(int descriptor, sockaddr_in* address, socklen_t addressLength)
    {
        if (address->sin_family != AF_INET)
        {
            return -1;
        }
        
        if (!Sockets.TryGetValue(descriptor, out var value) || value != null)
        {
            return -1;
        }

        var addrBytes = new byte[4];
        Marshal.Copy((IntPtr) address->sin_addr.s_addr, addrBytes, 0, 4);
        var ipAddress = BitConverter.ToUInt32(addrBytes);
        var port = address->sin_port;

        var client = new TcpClient(ipAddress, port);
        var socket = client.Connect();

        Sockets[descriptor] = socket;
        ReadQueues[descriptor] = new BlockingCollection<byte[]>(new ConcurrentQueue<byte[]>());

        socket.MessageReceived += data => ReadQueues[descriptor].Add(data);
        
        return 0;
    }

    [UnmanagedCallersOnly(EntryPoint = "write")]
    public static unsafe int Write(int descriptor, void* bufferPtr, int count)
    {
        if (descriptor < StartDescriptorValue)
        {
            return (int) write(descriptor, bufferPtr, count);
        }
        
        if (!Sockets.TryGetValue(descriptor, out var socket) || socket == null)
        {
            return -1;
        }

        var buffer = new byte[count];
        Marshal.Copy((IntPtr) bufferPtr, buffer, 0, count);
        
        return socket.Send(buffer);
    }

    [UnmanagedCallersOnly(EntryPoint = "read")]
    public static unsafe int Read(int descriptor, void* buffer, int count)
    {
        if (descriptor < StartDescriptorValue)
        {
            return (int) read(descriptor, buffer, count);
        }
        
        if (!Sockets.TryGetValue(descriptor, out var socket) || socket == null)
        {
            return -1;
        }

        var data = ReadQueues[descriptor].Take();
        var readBytes = data.Length < count ? data.Length : count;
        
        Marshal.Copy(data, 0, (IntPtr) buffer, readBytes);

        return readBytes;
    }
 
    [UnmanagedCallersOnly(EntryPoint = "close")]
    public static int Close(int descriptor)
    {
        if (descriptor < StartDescriptorValue)
        {
            return close(descriptor);
        }
        
        if (!Sockets.TryGetValue(descriptor, out var socket) || socket == null)
        {
            return -1;
        }
        
        socket.Disconnect();

        return 0;
    }
}
