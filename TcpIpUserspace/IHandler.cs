namespace TcpIpUserspace;

public interface IHandler<in T>
{
    void Handle(T t);
}