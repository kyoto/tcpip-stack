namespace TcpIpUserspace.Util;

public static class ChecksumUtils
{
    public static unsafe ushort Compute(byte[] buffer, int count, int startSum)
    {
        fixed (void* addr = buffer)
        {
            var sum = (uint)startSum;

            sum += SumEveryWord(addr, count);

            while (sum >> 16 != 0)
            {
                sum = (sum & 0xffff) + (sum >> 16);
            }

            return (ushort)~sum;
        }
    }

    public static ushort Compute(byte[] buffer)
    {
        return Compute(buffer, buffer.Length, 0);
    }

    private static unsafe uint SumEveryWord(void* addr, int count)
    {
        uint sum = 0;
        var ptr = (ushort*) addr;

        while (count > 1)
        {
            sum += *ptr++;
            count -= 2;
        }

        if (count > 0)
        {
            sum += *(byte*)ptr;
        }

        return sum;
    }
}