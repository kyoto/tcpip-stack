namespace TcpIpUserspace.Util;

public static class IpUtils
{
    public static string IpIntToString(uint ip)
    {
        var bytes = BitConverter.GetBytes(ip);

        return string.Join('.', bytes);
    }
}