using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;

namespace TcpIpUserspace.Util;

public static class ByteUtils
{
    public static byte[] StructToByteArray(object obj)
    {
        var size = Marshal.SizeOf(obj);
        var result = new byte[size];
        var ptr = Marshal.AllocHGlobal(size);
        var type = obj.GetType();
        
        var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
            .Where(f => f.GetCustomAttributes(typeof(NetworkEndiannessAttribute), false).Length != 0)
            .ToList();

        foreach (var field in fields)
        {
            var value = field.GetValue(obj)!;
            field.SetValue(obj, HostToNetworkOrder(value));
        }
        
        Marshal.StructureToPtr(obj, ptr, true);
        Marshal.Copy(ptr, result, 0, size);
        
        Marshal.FreeHGlobal(ptr);

        return result;
    }

    public static T ByteArrayToStruct<T>(byte[] bytes, out byte[] remainder)
    {
        var type = typeof(T);
        var size = Marshal.SizeOf(type);
        
        var pointer = Marshal.AllocHGlobal(size);
        Marshal.Copy(bytes, 0, pointer, size);

        remainder = new byte[bytes.Length - size];
        Array.Copy(bytes, size, remainder, 0, remainder.Length);
        
        var result = Marshal.PtrToStructure(pointer, type);

        if (result == null)
        {
            throw new NullReferenceException($"Could not convert byte array to type {type.Name}");
        }

        var fields = type.GetFields()
            .Where(f => f.GetCustomAttributes(typeof(NetworkEndiannessAttribute), false).Length != 0)
            .ToList();

        foreach (var field in fields)
        {
            var value = field.GetValue(result)!;
            field.SetValue(result, NetworkToHostOrder(value));
        }
        
        Marshal.FreeHGlobal(pointer);

        return (T) result;
    }

    private static object NetworkToHostOrder(object obj)
    {
        if (obj is Enum)
        {
            obj = EnumUnderlyingValue(obj);
        }
        
        return obj switch
        {
            ushort n => NetworkToHostOrder(n),
            uint n => NetworkToHostOrder(n),
            ulong n => NetworkToHostOrder(n),
            short n => IPAddress.NetworkToHostOrder(n),
            int n => IPAddress.NetworkToHostOrder(n),
            long n => IPAddress.NetworkToHostOrder(n),
            _ => throw new NotSupportedException("Argument must be an integer number")
        };
    }
    
    private static object HostToNetworkOrder(object obj)
    {
        if (obj is Enum)
        {
            obj = EnumUnderlyingValue(obj);
        }
        
        return obj switch
        {
            ushort n => HostToNetworkOrder(n),
            uint n => HostToNetworkOrder(n),
            ulong n => HostToNetworkOrder(n),
            short n => IPAddress.HostToNetworkOrder(n),
            int n => IPAddress.HostToNetworkOrder(n),
            long n => IPAddress.HostToNetworkOrder(n),
            _ => throw new NotSupportedException("Argument must be an integer number")
        };
    }

    private static object EnumUnderlyingValue(object enumValue)
    {
        var type = Enum.GetUnderlyingType(enumValue.GetType());
        return Convert.ChangeType(enumValue, type);
    }

    public static ushort HostToNetworkOrder(ushort hostShort)
    {
        return (ushort)IPAddress.HostToNetworkOrder((short)hostShort);
    }

    public static ushort NetworkToHostOrder(ushort networkShort)
    {
        return (ushort)IPAddress.NetworkToHostOrder((short)networkShort);
    }
    
    public static uint HostToNetworkOrder(uint hostShort)
    {
        return (uint)IPAddress.HostToNetworkOrder((int)hostShort);
    }

    public static uint NetworkToHostOrder(uint networkShort)
    {
        return (uint)IPAddress.NetworkToHostOrder((int)networkShort);
    }
    
    public static ulong HostToNetworkOrder(ulong hostShort)
    {
        return (ulong)IPAddress.HostToNetworkOrder((long)hostShort);
    }

    public static ulong NetworkToHostOrder(ulong networkShort)
    {
        return (ulong)IPAddress.NetworkToHostOrder((long)networkShort);
    }
    
    public static byte[] ConcatByteArrays(params byte[][] arrays)
    {
        var totalLength = arrays.Aggregate(0, (i, bytes) => i + bytes.Length);
        var buffer = new byte[totalLength];

        var start = 0;
        foreach (var array in arrays)
        {
            Array.Copy(array, 0, buffer, start, array.Length);
            start += array.Length;
        }

        return buffer;
    }

    public static (byte[], byte[]) SplitBuffer(byte[] buffer, int splitIndex)
    {
        var first = buffer.Take(splitIndex).ToArray();
        var second = buffer.Skip(splitIndex).ToArray();

        return (first, second);
    }
}

[AttributeUsage(AttributeTargets.Field)]
public class NetworkEndiannessAttribute : Attribute
{}
