using System.Net;
using TcpIpUserspace.Ethernet;
using TcpIpUserspace.TunTap;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Arp;

public class ArpHandler : IHandler<EthernetFrame>
{
    private const ushort ArpEthernet = 0x0001;
    private const ushort ArpIpv4 = 0x0800;
    private const ushort ArpRequest = 0x0001;
    private const ushort ArpReply = 0x0002;

    public static readonly Dictionary<uint, byte[]> Cache = new();

    public ArpHandler()
    {
        var reservedAddresses = new[] { "10.0.0.1", "10.0.0.2" };
        var device = TapDevice.GetInstance();
        var deviceMac = device.Interface.GetPhysicalAddress().GetAddressBytes();

        foreach (var addr in reservedAddresses)
        {
            var intAddress = BitConverter.ToUInt32(IPAddress.Parse(addr).GetAddressBytes());
            intAddress = ByteUtils.HostToNetworkOrder(intAddress);
            Cache[intAddress] = deviceMac;
        }
    }

    public void Handle(EthernetFrame frame)
    {
        var header = ByteUtils.ByteArrayToStruct<ArpHeader>(frame.Payload, out var dataBuffer);

        if (header.HardwareType != ArpEthernet)
        {
            throw new NotSupportedException($"Unsupported hardware type ({header.HardwareType})");
        }
        
        if (header.ProtocolType != ArpIpv4)
        {
            throw new NotSupportedException($"Unsupported protocol type ({header.ProtocolType})");
        }
        
        if (header.Operation != ArpRequest)
        {
            throw new NotSupportedException($"Unsupported operation (opcode {header.Operation})");
        }
        
        var data = ByteUtils.ByteArrayToStruct<ArpIpV4>(dataBuffer, out _);
        var device = TapDevice.GetInstance();

        if (data.DestinationIp != device.IpAddress)
        {
            Console.Error.WriteLine($"Mocking ARP reply for {IpUtils.IpIntToString(data.DestinationIp)}");
        }

        Cache[data.SourceIp] = data.SourceMac;
        Reply(frame, header, data);
    }

    private void Reply(EthernetFrame frame, ArpHeader header, ArpIpV4 data)
    {
        var device = TapDevice.GetInstance();
        var deviceMac = device.Interface.GetPhysicalAddress().GetAddressBytes();
        
        header.Operation = ArpReply;

        data.DestinationMac = data.SourceMac;
        data.SourceMac = deviceMac;
        
        // TODO SourceIp must be actual device IP, fix when remove arp mock
        (data.DestinationIp, data.SourceIp) = (data.SourceIp, data.DestinationIp);

        frame.Reply(EtherType.Arp, CreatePayload(header, data));
    }

    private static byte[] CreatePayload(ArpHeader header, ArpIpV4 data)
    {
        var headerPayload = ByteUtils.StructToByteArray(header);
        var dataPayload = ByteUtils.StructToByteArray(data);

        var payload = ByteUtils.ConcatByteArrays(headerPayload, dataPayload);

        return payload;
    }
}