using System.Runtime.InteropServices;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Arp;

[StructLayout(LayoutKind.Sequential, Pack = 1)]

public struct ArpHeader
{
    [NetworkEndianness]
    public ushort HardwareType;
    
    [NetworkEndianness]
    public ushort ProtocolType;
    
    public byte HardwareSize;
    
    public byte ProtocolSize;
    
    [NetworkEndianness]
    public ushort Operation;
}