using System.Runtime.InteropServices;

namespace TcpIpUserspace.Arp;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct ArpIpV4
{
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    public byte[] SourceMac;

    public uint SourceIp;
    
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    public byte[] DestinationMac;

    public uint DestinationIp;
}
