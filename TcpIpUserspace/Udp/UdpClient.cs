using System.Collections.Concurrent;

namespace TcpIpUserspace.Udp;

public class UdpClient
{
    private readonly ushort _port;
    
    private static readonly ConcurrentDictionary<ushort, BlockingCollection<UdpDatagram>> Queues = new();
    
    public UdpClient(ushort port)
    {
        _port = port;
        Queues[_port] = new BlockingCollection<UdpDatagram>(new ConcurrentQueue<UdpDatagram>());
    }
    
    public UdpDatagram Receive()
    {
        return Queues[_port].Take();
    }

    internal static void Enqueue(ushort port, UdpDatagram datagram)
    {
        if (!Queues.ContainsKey(port))
        {
            Queues[port] = new BlockingCollection<UdpDatagram>(new ConcurrentQueue<UdpDatagram>());
        }
        
        Queues[port].Add(datagram);
    }
}