using System.Collections.Concurrent;
using TcpIpUserspace.Ip;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Udp;

public class UdpHandler : IHandler<IpDatagram>
{
    public void Handle(IpDatagram ipDatagram)
    {
        var header = ByteUtils.ByteArrayToStruct<UdpHeader>(ipDatagram.Payload, out var data);
        var datagram = new UdpDatagram(ipDatagram, header, data);

        UdpClient.Enqueue(header.DestinationPort, datagram);
    }
}