using System.Runtime.InteropServices;
using TcpIpUserspace.Ip;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Udp;

public record UdpDatagram(IpDatagram IpDatagram, UdpHeader Header, byte[] Data)
{
    public void Reply(byte[] payload)
    {
        var replyHeader = new UdpHeader
        {
            SourcePort = Header.DestinationPort,
            DestinationPort = Header.SourcePort,
            Checksum = 0,
            Length = (ushort)(Marshal.SizeOf<UdpHeader>() + Data.Length)
        };

        var datagram = CreateDatagram(replyHeader, payload);
        
        IpDatagram.Reply(IpProtocol.Udp, datagram);
    }

    private byte[] CreateDatagram(UdpHeader header, byte[] payload)
    {
        var buffer = ByteUtils.ConcatByteArrays(
            ByteUtils.StructToByteArray(header),
            payload
        );

        header.Checksum = ChecksumUtils.Compute(buffer, buffer.Length, 0);
        
        buffer = ByteUtils.ConcatByteArrays(
            ByteUtils.StructToByteArray(header),
            payload
        );

        return buffer;
    }
}