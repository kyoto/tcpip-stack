using System.Runtime.InteropServices;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Udp;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct UdpHeader
{
    [NetworkEndianness]
    public ushort SourcePort;
    
    [NetworkEndianness]
    public ushort DestinationPort;

    [NetworkEndianness]
    public ushort Length;

    public ushort Checksum;
}
