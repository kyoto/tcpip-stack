namespace TcpIpUserspace.Tcp;

public class TcpSenderSequence
{
    public int Unacknowledged { get; set; }
    public int Next { get; set; }
    public int Initial { get; }

    public TcpSenderSequence()
    {
        var rand = new Random(Guid.NewGuid().GetHashCode());
        
        Initial = rand.Next();

        Next = Initial;
        Unacknowledged = Initial;
    }
}