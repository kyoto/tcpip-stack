namespace TcpIpUserspace.Tcp;

public class TcpClient(uint address, ushort port) : ITcpReceiver
{
    private readonly TcpSocket _socket = new(address, GetClientPort(), port);
    private static Random _rand = new();

    public TcpSocket Connect()
    {
        var connectionEvent = new ManualResetEvent(false);
        _socket.ClientConnected += (_, _) =>
        {
            connectionEvent.Set();
        };
        _socket.Connect();

        connectionEvent.WaitOne();

        return _socket;
    }

    public void Receive(TcpPacket packet)
    {
        _socket.Handle(packet);
    }

    private static ushort GetClientPort()
    {
        return (ushort)_rand.Next(49152, 65536);
    }
}