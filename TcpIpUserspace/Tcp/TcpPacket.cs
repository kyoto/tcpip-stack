using System.Data;
using System.Runtime.InteropServices;
using TcpIpUserspace.Ip;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Tcp;

public record TcpPacket
{
    private readonly IpHeader _ipHeader;
    private TcpHeader _tcpHeader;
    private readonly TcpOption[] _options;
    private readonly byte[] _data;

    public TcpHeader Header => _tcpHeader;
    public byte[] Data => _data;
    public uint SourceAddress => _ipHeader.SourceAddress;
    public ushort SourcePort => _tcpHeader.SourcePort;

    public TcpPacket(IpHeader ipHeader, TcpHeader header, TcpOption[] options, byte[]? data = null)
    {
        _ipHeader = ipHeader;
        _tcpHeader = header;
        _options = options;
        _data = data ?? Array.Empty<byte>();
    }

    public TcpPacket(IpDatagram ipDatagram, TcpHeader header, TcpOption[] options, byte[]? data = null)
    {
        _ipHeader = ipDatagram.Header;
        _tcpHeader = header;
        _options = options;
        _data = data ?? Array.Empty<byte>();
    }

    public void Send()
    {
        var packedOptions = TcpOption.Pack(_options);
        var buffer = ByteUtils.ConcatByteArrays(
            ByteUtils.StructToByteArray(_tcpHeader),
            packedOptions,
            _data
        );

        var pseudoHeader = new PseudoIpHeader(
            _ipHeader.SourceAddress,
            _ipHeader.DestinationAddress,
            IpProtocol.Tcp,
            (ushort) buffer.Length
        );
        _tcpHeader.Checksum = GetChecksum(pseudoHeader, buffer);

        buffer = ByteUtils.ConcatByteArrays(
            ByteUtils.StructToByteArray(_tcpHeader),
            packedOptions,
            _data
        );

        var datagram = new IpDatagram(_ipHeader, buffer);
        datagram.Send(false);
    }
    
    public void Reply(int sequence, int acknowledgement, TcpOption[] options, TcpFlags flags, byte[]? data = null)
    {
        var packedOptions = TcpOption.Pack(options);

        data ??= Array.Empty<byte>();

        var replyHeader = new TcpHeader(_tcpHeader.DestinationPort, _tcpHeader.SourcePort, sequence, acknowledgement,
            packedOptions.Length, flags);
        
        var buffer = ByteUtils.ConcatByteArrays(
            ByteUtils.StructToByteArray(_tcpHeader),
            packedOptions,
            data
        );

        var replyPacket = new TcpPacket(_ipHeader.CreateReplyHeader(buffer.Length + Marshal.SizeOf<IpHeader>()), replyHeader, options, data);
        replyPacket.Send();
    }

    public bool VerifyChecksum()
    {
        var packedOptions = TcpOption.Pack(_options);
        var buffer = ByteUtils.ConcatByteArrays(
            ByteUtils.StructToByteArray(_tcpHeader),
            packedOptions,
            _data
        );
        var pseudoHeader = new PseudoIpHeader(
            _ipHeader.SourceAddress,
            _ipHeader.DestinationAddress,
            IpProtocol.Tcp,
            (ushort) (buffer.Length)
        );
        
        return GetChecksum(pseudoHeader, buffer) == 0;
    }

    private static ushort GetChecksum(PseudoIpHeader pseudoHeader, byte[] payload)
    {
        var pseudoHeaderBuffer = ByteUtils.StructToByteArray(pseudoHeader);

        var buffer = ByteUtils.ConcatByteArrays(
            pseudoHeaderBuffer,
            payload
        );

        return ByteUtils.NetworkToHostOrder(ChecksumUtils.Compute(buffer, buffer.Length, 0));
    }
}