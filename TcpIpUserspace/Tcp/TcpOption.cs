namespace TcpIpUserspace.Tcp;

public enum TcpOptionKind : byte
{
    End = 0,
    NoOperation = 1,
    MaximumSegmentSize = 2,
    WindowScale = 3,
    SelectiveAcknowledgementPermitted = 4,
    SelectiveAcknowledgement = 5,
    Timestamps = 8,
}
public record TcpOption(TcpOptionKind Kind, byte[]? Data)
{
    public static TcpOption[] ReadFromBuffer(byte[] optionsBuffer)
    {
        var result = new List<TcpOption>();
        
        for (var i = 0; i < optionsBuffer.Length;)
        {
            var kindNumber = optionsBuffer[i++];

            if (!Enum.IsDefined(typeof(TcpOptionKind), kindNumber))
            {
                ++i; // Skip length, all standard options except end and noop have it
                continue;
            }

            var kind = (TcpOptionKind)kindNumber;

            if (kind == TcpOptionKind.End)
            {
                break;
            }

            if (kind == TcpOptionKind.NoOperation)
            {
                continue;
            }

            var length = optionsBuffer[i++] - 2;
            var data = length == 0 ? null : optionsBuffer.Skip(i).Take(length).ToArray();
            
            result.Add(new TcpOption(kind, data));

            i += length;
        }

        return result.ToArray();
    }

    public static byte[] Pack(TcpOption[] options)
    {
        if (options.Length == 0)
        {
            return Array.Empty<byte>();
        }
        
        var buffer = new List<byte>(options.Length * 3);

        foreach (var option in options)
        {
            buffer.Add((byte)option.Kind);
            buffer.Add((byte)(option.Data?.Length ?? 0 + 2));

            if (option.Data != null)
            {
                buffer.AddRange(option.Data);
            }
        }
        
        buffer.Add((byte)TcpOptionKind.End);

        var padding = (4 - buffer.Count % 4) % 4;

        while (padding-- != 0)
        {
            buffer.Add(0);
        }

        return buffer.ToArray();
    }
}