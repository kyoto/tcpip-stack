using System.Runtime.InteropServices;
using TcpIpUserspace.Ip;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Tcp;

public class TcpHandler : IHandler<IpDatagram>
{
    public void Handle(IpDatagram datagram)
    {
        var header = ByteUtils.ByteArrayToStruct<TcpHeader>(datagram.Payload, out var remaining);

        if (header.DataOffset < 5)
        {
            throw new NotSupportedException("TCP header data offset must be at least 5 d-words");
        }

        var (optionsBuffer, payload) = ByteUtils.SplitBuffer(remaining, header.DataOffset * 4 - Marshal.SizeOf<TcpHeader>());
        var options = TcpOption.ReadFromBuffer(optionsBuffer);

        var packet = new TcpPacket(datagram, header, options, payload);
        
        ITcpReceiver.Handle(packet);
    }
}