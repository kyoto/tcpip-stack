using System.Collections.Concurrent;

namespace TcpIpUserspace.Tcp;

public interface ITcpReceiver
{
    private static readonly ConcurrentDictionary<ushort, ITcpReceiver> PortMap = new();
    
    void Receive(TcpPacket packet);

    public static void Register(ushort port, ITcpReceiver receiver)
    {
        if (PortMap.ContainsKey(port))
        {
            throw new Exception($"Port {port} is already in use");
        }
        
        PortMap[port] = receiver;
    }
    
    public static void Handle(TcpPacket packet)
    {
        var port = packet.Header.DestinationPort;
        
        if (!PortMap.ContainsKey(port))
        {
            packet.Reply(
                0,
                packet.Header.Sequence + 1,
                Array.Empty<TcpOption>(),
                TcpFlags.Rst | TcpFlags.Ack
            );
            return;
        }
        
        PortMap[packet.Header.DestinationPort].Receive(packet);
    }
}