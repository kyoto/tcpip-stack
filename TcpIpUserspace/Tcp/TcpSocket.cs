using System.Net;
using System.Runtime.InteropServices;
using TcpIpUserspace.Ip;
using TcpIpUserspace.TunTap;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Tcp;

public class TcpSocket(uint address, ushort sourcePort, ushort destinationPort)
{
    private readonly TcpSenderSequence _sender = new();
    private readonly TcpReceiverSequence _receiver = new();

    private bool _shouldSyn = true;

    public delegate void MessageHandler(byte[] data);

    public delegate void TcpClientHandler(uint address, ushort port);

    public event MessageHandler? MessageReceived;
    public event TcpClientHandler? ClientDisconnected;
    public event TcpClientHandler? ClientConnected;
    
    internal void Handle(TcpPacket packet)
    {
        var header = packet.Header;
        var flags = TcpFlags.Ack;
        var options = Array.Empty<TcpOption>();
        
        var ghostByte = 0;
        if (header.Syn)
        {
            _receiver.Initial = header.Sequence;
            ghostByte = 1;
        }

        if (_shouldSyn)
        {
            flags |= TcpFlags.Syn;
            _shouldSyn = false;
        }

        if (header.Syn || header.Psh)
        {
            _receiver.Next = header.Sequence + packet.Data.Length + ghostByte;
            packet.Reply(_sender.Next, _receiver.Next, options, flags);
            _sender.Next += ghostByte;
            
            ClientConnected?.Invoke(address, destinationPort);
        }

        if (header.Ack)
        {
            _sender.Unacknowledged = header.Acknowledgement + 1;
        }

        if (header.Fin)
        {
            Disconnect();
        }

        if (packet.Data.Length != 0)
        {
            MessageReceived?.Invoke(packet.Data);
        }
    }

    public void Connect()
    {
        var options = Array.Empty<TcpOption>();

        const TcpFlags flags = TcpFlags.Psh | TcpFlags.Ack;

        var length = (ushort) (Marshal.SizeOf<TcpHeader>() + Marshal.SizeOf<IpHeader>());
        var tcpHeader = new TcpHeader(sourcePort, destinationPort, _sender.Next, _receiver.Next, 0, flags);
        var ipHeader = new IpHeader(length, IpProtocol.Tcp, TapDevice.DefaultAddress(), address);
        var packet = new TcpPacket(ipHeader, tcpHeader, options);

        packet.Send();
    }

    public int Send(byte[] data)
    {
        var options = Array.Empty<TcpOption>();

        const TcpFlags flags = TcpFlags.Psh | TcpFlags.Ack;

        var length = (ushort) (data.Length + Marshal.SizeOf<TcpHeader>() + Marshal.SizeOf<IpHeader>());
        var tcpHeader = new TcpHeader(sourcePort, destinationPort, _sender.Next, _receiver.Next, 0, flags);
        var ipHeader = new IpHeader(length, IpProtocol.Tcp, TapDevice.DefaultAddress(), address);
        var packet = new TcpPacket(ipHeader, tcpHeader, options, data);

        packet.Send();
        _sender.Next += data.Length;

        return data.Length;
    }
    
    public void Disconnect()
    {
        var options = Array.Empty<TcpOption>();

        const TcpFlags flags = TcpFlags.Rst;

        var length = (ushort) (Marshal.SizeOf<TcpHeader>() + Marshal.SizeOf<IpHeader>());
        var tcpHeader = new TcpHeader(sourcePort, destinationPort, _sender.Next, _receiver.Next, 0, flags);
        var ipHeader = new IpHeader(length, IpProtocol.Tcp, TapDevice.DefaultAddress(), address);
        var packet = new TcpPacket(ipHeader, tcpHeader, options);
        
        packet.Send();
        ClientDisconnected?.Invoke(address, destinationPort);
    }
    
    public override string ToString()
    {
        return SocketAddress(address, destinationPort);
    }

    public static string SocketAddress(uint address, ushort port)
    {
        var ipString = new IPAddress(ByteUtils.NetworkToHostOrder(address)).ToString();
        return $"{ipString}:{port}";
    }
}