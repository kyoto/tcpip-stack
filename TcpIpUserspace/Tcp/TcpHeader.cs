using System.Runtime.InteropServices;
using System.Security.Authentication.ExtendedProtection;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Tcp;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct TcpHeader
{
    [NetworkEndianness]
    public ushort SourcePort;
    [NetworkEndianness]
    public ushort DestinationPort;

    [NetworkEndianness]
    public int Sequence;

    [NetworkEndianness]
    public int Acknowledgement;

    private byte _dataOffset;
    public byte DataOffset
    {
        get => (byte)(_dataOffset >> 4);
        set => _dataOffset = (byte)(value << 4);
    }
    [MarshalAs(UnmanagedType.U1)]
    public TcpFlags Flags;
    [NetworkEndianness]
    public ushort WindowSize;

    [NetworkEndianness]
    public ushort Checksum;
    [NetworkEndianness]
    public ushort UrgentPointer;

    public TcpHeader(ushort sPort, ushort dPort, int seq, int ack, int optionsLength, TcpFlags flags)
    {
        SourcePort = sPort;
        DestinationPort = dPort;
        Sequence = seq;
        Acknowledgement = ack;
        DataOffset = (byte)((optionsLength + Marshal.SizeOf<TcpHeader>()) / 4);
        Flags = flags;
        WindowSize = ushort.MaxValue;
        Checksum = 0;
        UrgentPointer = 0;
    }

    public bool Fin => Flags.HasFlag(TcpFlags.Fin);
    public bool Syn => Flags.HasFlag(TcpFlags.Syn);
    public bool Rst => Flags.HasFlag(TcpFlags.Rst);
    public bool Psh => Flags.HasFlag(TcpFlags.Psh);
    public bool Ack => Flags.HasFlag(TcpFlags.Ack);
    public bool Urg => Flags.HasFlag(TcpFlags.Urg);
    public bool Ece => Flags.HasFlag(TcpFlags.Ece);
    public bool Cwr => Flags.HasFlag(TcpFlags.Cwr);
}