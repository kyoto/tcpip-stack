using System.Collections.Concurrent;
using System.Net;

namespace TcpIpUserspace.Tcp;

public class TcpServer : ITcpReceiver
{
    private ushort _port;

    private readonly Dictionary<string, TcpSocket> _clients = new();

    public delegate void TcpClientHandler(TcpSocket client);

    public event TcpClientHandler? ClientConnected;

    public TcpServer(ushort port)
    {
        _port = port;
        
        ITcpReceiver.Register(port, this);
    }

    public void Receive(TcpPacket packet)
    {
        var address = TcpSocket.SocketAddress(packet.SourceAddress, packet.SourcePort);

        if (!_clients.ContainsKey(address))
        {
            if (!packet.Header.Syn)
            {
                return;
            }
            
            _clients[address] = new TcpSocket(packet.SourceAddress, _port, packet.SourcePort);

            TcpSocket.TcpClientHandler? handler = null;
            _clients[address].ClientConnected += handler = (_, _) =>
            {
                ClientConnected?.Invoke(_clients[address]);
                _clients[address].ClientConnected -= handler!;
            };
            
            _clients[address].ClientDisconnected += (ip, port) =>
            {
                _clients.Remove(TcpSocket.SocketAddress(ip, port));
            };
        }

        var header = packet.Header;

        if (header.Rst)
        {
            _clients[address].Disconnect();
            return;
        }
        
        _clients[address].Handle(packet);
    }
}