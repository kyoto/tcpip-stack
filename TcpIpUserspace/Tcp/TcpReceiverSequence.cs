namespace TcpIpUserspace.Tcp;

public class TcpReceiverSequence
{
    public int Next { get; set; }
    public int Initial { get; set; }
}