using TcpIpUserspace.Arp;
using TcpIpUserspace.Ip;
using TcpIpUserspace.TunTap;

namespace TcpIpUserspace.Ethernet;

public class EthernetHandler : IHandler<EthernetFrame>
{
    private readonly Dictionary<EtherType, IHandler<EthernetFrame>> _strategy = new();

    public EthernetHandler()
    {
        _strategy[EtherType.Arp] = new ArpHandler();
        _strategy[EtherType.Ip] = new IpDatagramHandler();
    }
    
    public void Handle(EthernetFrame frame)
    {
        if (_strategy.TryGetValue(frame.Header.EtherType, out var value))
        {
            value.Handle(frame);
        }
    }

    public void Listen()
    {
        var thread = new Thread(ThreadStart);
        thread.Start();
    }

    private void ThreadStart()
    {
        while (EthernetFrame.Read(out var frame))
        {
            try
            {
                Handle(frame);
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
            }
        }
    }
}