using TcpIpUserspace.TunTap;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Ethernet;

public struct EthernetFrame
{
    private const int MaxSize = 1518;
    
    public EthernetHeader Header;
    public byte[] Payload;

    public static bool Read(out EthernetFrame result)
    {
        var device = TapDevice.GetInstance();
        var buffer = new byte[MaxSize];
        var bytesRead = device.Stream.Read(buffer);

        buffer = buffer.Take(bytesRead).ToArray();

        result = new EthernetFrame();
        if (bytesRead == 0)
        {
            return false;
        } 
        
        result.Header = ByteUtils.ByteArrayToStruct<EthernetHeader>(buffer, out result.Payload);

        return true;
    }

    public void Send()
    {
        var headerBuffer = ByteUtils.StructToByteArray(Header);
        var frameBuffer = ByteUtils.ConcatByteArrays(headerBuffer, Payload);
        var tapDevice = TapDevice.GetInstance();
        
        tapDevice.Stream.Write(frameBuffer);
        tapDevice.Stream.Flush();
    }

    public void Reply(EtherType etherType, byte[] payload)
    {
        var header = Header.GetReplyHeader(etherType);
        var frame = new EthernetFrame()
        {
            Header = header,
            Payload = payload,
        };

        frame.Send();
    }
}
