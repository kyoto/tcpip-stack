using System.Runtime.InteropServices;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Ethernet;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct EthernetHeader
{
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    public byte[] DestinationMac;
    
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
    public byte[] SourceMac;

    [NetworkEndianness]
    [MarshalAs(UnmanagedType.U2)]
    public EtherType EtherType;

    public EthernetHeader GetReplyHeader(EtherType etherType)
    {
        return new EthernetHeader
        {
            SourceMac = DestinationMac,
            DestinationMac = SourceMac,
            EtherType = etherType
        };
    }
}