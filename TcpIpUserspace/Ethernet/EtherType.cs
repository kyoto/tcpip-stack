namespace TcpIpUserspace.Ethernet;

public enum EtherType : ushort
{
    Ip = 0x0800,
    Arp = 0x0806
}