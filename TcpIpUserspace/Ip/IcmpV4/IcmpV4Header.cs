using System.Runtime.InteropServices;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Ip.IcmpV4;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct IcmpV4Header
{
    [MarshalAs(UnmanagedType.U1)]
    public IcmpV4Type Type;
    
    public byte Code;
    
    public ushort Checksum;

    public void CalculateChecksum(byte[] data)
    {
        Checksum = 0;
        
        var headerBuffer = ByteUtils.StructToByteArray(this);
        var buffer = ByteUtils.ConcatByteArrays(headerBuffer, data);

        Checksum = ChecksumUtils.Compute(buffer, buffer.Length, 0);
    }
}

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct IcmpV4Echo
{
    [NetworkEndianness]
    public ushort Id;
    
    [NetworkEndianness]
    public ushort Sequence;
}
