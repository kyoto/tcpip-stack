using TcpIpUserspace.Util;

namespace TcpIpUserspace.Ip.IcmpV4;

public class IcmpV4Handler : IHandler<IpDatagram>
{
    public void Handle(IpDatagram datagram)
    {
        var header = ByteUtils.ByteArrayToStruct<IcmpV4Header>(datagram.Payload, out var data);

        switch (header.Type)
        {
            case IcmpV4Type.EchoRequest:
                Reply(datagram, data);
                break;
            default:
                Console.Error.WriteLine("Unsupported ICMP request");
                break;
        }
    }

    private void Reply(IpDatagram datagram, byte[] dataBuffer)
    {
        var header = new IcmpV4Header()
        {
            Type = IcmpV4Type.EchoReply,
            Code = 0,
            Checksum = 0
        };
        
        header.CalculateChecksum(dataBuffer);

        var icmpHeaderBuffer = ByteUtils.StructToByteArray(header);
        var payload = ByteUtils.ConcatByteArrays(icmpHeaderBuffer, dataBuffer);

        datagram.Reply(IpProtocol.IcmpV4, payload);
    }
}