namespace TcpIpUserspace.Ip.IcmpV4;

public enum IcmpV4Type : byte
{
    EchoRequest = 0x08,
    EchoReply = 0x00,
    DestinationUnreachable = 0x03
}
