namespace TcpIpUserspace.Ip;

public enum IpProtocol : byte
{
    IcmpV4 = 0x01,
    Tcp = 0x06,
    Udp = 0x11
}