using System.Runtime.InteropServices;
using TcpIpUserspace.Ethernet;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Ip;

public class IpDatagram
{
    public readonly byte[] Payload;
    public IpHeader Header;

    private EthernetHeader _ethernetHeader;

    public IpDatagram(EthernetHeader ethernetHeader, IpHeader ipHeader, byte[] payload)
    {
        Payload = payload;
        Header = ipHeader;
        _ethernetHeader = ethernetHeader;
    }

    public IpDatagram(IpHeader header, byte[] payload)
    {
        _ethernetHeader = header.CreateEthernetHeader();
        Payload = payload;
        Header = header;
    }

    public void Send(bool calcPayloadChecksum = true)
    {
        int? checksumCount = calcPayloadChecksum ? null : Marshal.SizeOf<IpHeader>();
        Header.Checksum = ByteUtils.NetworkToHostOrder(CalculateChecksum(Header, Payload, checksumCount));
        
        var buffer = ByteUtils.ConcatByteArrays(
            ByteUtils.StructToByteArray(Header),
            Payload
        );
        
        var frame = new EthernetFrame()
        {
            Header = _ethernetHeader,
            Payload = buffer
        };

        frame.Send();
    }

    public void Reply(IpProtocol protocol, byte[] payload, bool calcPayloadChecksum = true)
    {
        var length = (ushort)(Marshal.SizeOf<IpHeader>() + payload.Length);
        var replyHeader = new IpHeader(length, protocol, Header.DestinationAddress, Header.SourceAddress);
        var datagram = new IpDatagram(_ethernetHeader.GetReplyHeader(EtherType.Ip), replyHeader, payload);
        
        datagram.Send(calcPayloadChecksum);
    }

    private static ushort CalculateChecksum(IpHeader header, byte[] payload, int? count = null)
    {
        var headerBuffer = ByteUtils.StructToByteArray(header);
        var buffer = ByteUtils.ConcatByteArrays(headerBuffer, payload);

        count ??= buffer.Length;
        
        return ChecksumUtils.Compute(buffer, count.Value, 0);
    }
}