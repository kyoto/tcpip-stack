using TcpIpUserspace.Ethernet;
using TcpIpUserspace.Ip.IcmpV4;
using TcpIpUserspace.Tcp;
using TcpIpUserspace.Udp;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Ip;

public class IpDatagramHandler : IHandler<EthernetFrame>
{
    private readonly Dictionary<IpProtocol, IHandler<IpDatagram>> _strategy = new();
    
    private const byte IpV4 = 0x04;
    public IpDatagramHandler()
    {
        _strategy[IpProtocol.IcmpV4] = new IcmpV4Handler();
        _strategy[IpProtocol.Tcp] = new TcpHandler();
        _strategy[IpProtocol.Udp] = new UdpHandler();
    }
    
    public void Handle(EthernetFrame frame)
    {
        var header = ByteUtils.ByteArrayToStruct<IpHeader>(frame.Payload, out var payload);

        if (header.Version != IpV4)
        {
            throw new NotSupportedException($"Unsupported IP datagram version {header.Version}");
        }
        
        if (header.InternetHeaderLength < 5)
        {
            throw new NotSupportedException($"IPv4 header length must be at least 5 (got {header.InternetHeaderLength})");
        }
        
        if (header.TimeToLive == 0)
        {
            Console.Error.WriteLine("Datagram TTL reached 0");
            return;
        }

        var checksum = ChecksumUtils.Compute(frame.Payload, header.InternetHeaderLength * 4, 0);

        if (checksum != 0)
        {
            Console.Error.WriteLine("Corrupted IP datagram");
            return;
        }

        var datagram = new IpDatagram(frame.Header, header, payload);

        if (_strategy.TryGetValue(header.Protocol, out var value))
        {
            value.Handle(datagram);
        }
    }
}