namespace TcpIpUserspace.Ip;

[Flags]
public enum IpFlags: ushort
{
    Reserved = 0b100,
    DoNotFragment = 0b010,
    MoreFragments = 0b001
}