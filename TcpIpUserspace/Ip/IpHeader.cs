using System.Runtime.InteropServices;
using TcpIpUserspace.Arp;
using TcpIpUserspace.Ethernet;
using TcpIpUserspace.Util;

namespace TcpIpUserspace.Ip;

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct IpHeader
{
    private const ushort FlagsMask = 0b1110000000000000;
    private const int FlagsOffset = 13;
    
    private byte _versionIhl;

    public byte TypeOfService;

    [NetworkEndianness]
    public ushort Length;
    
    [NetworkEndianness]
    public ushort Id;
    
    [NetworkEndianness]
    private ushort _flagsFragOffset;

    public byte TimeToLive;
    public IpProtocol Protocol;
    
    [NetworkEndianness]
    public ushort Checksum;
    
    [NetworkEndianness]
    public uint SourceAddress;
    
    [NetworkEndianness]
    public uint DestinationAddress;
    public byte Version
    {
        get => (byte)((_versionIhl & 0xf0) >> 4);
        set => _versionIhl = (byte) ((_versionIhl & 0x0f) | ((value & 0x0f) << 4));
    }
    public byte InternetHeaderLength
    {
        get => (byte)(_versionIhl & 0x0f);
        set => _versionIhl = (byte) ((_versionIhl & 0xf0) | (value & 0x0f));
    }

    public IpFlags Flags
    {
        get => (IpFlags)((_flagsFragOffset & FlagsMask) >> FlagsOffset);
        set => _flagsFragOffset = (ushort)((_flagsFragOffset & ~FlagsMask) | ((ushort)value << FlagsOffset));
    }

    public ushort FragmentOffset
    {
        get => (ushort)(_flagsFragOffset & ~FlagsMask);
        set => _flagsFragOffset = (ushort)((_flagsFragOffset & FlagsMask) | (value & ~FlagsMask));
    }

    public IpHeader(ushort length, IpProtocol protocol, uint sourceAddress, uint destinationAddress)
    {
        Version = 4;
        InternetHeaderLength = 5;
        TypeOfService = 0;
        Length = length;
        Id = 0;
        Flags = IpFlags.DoNotFragment;
        FragmentOffset = 0;
        TimeToLive = 64;
        Protocol = protocol;
        Checksum = 0;
        SourceAddress = sourceAddress;
        DestinationAddress = destinationAddress;
    }

    public EthernetHeader CreateEthernetHeader()
    {
        if (!ArpHandler.Cache.ContainsKey(SourceAddress) || !ArpHandler.Cache.ContainsKey(DestinationAddress))
        {
            throw new Exception("Address could not be resolved");
        }

        var dstMac = ArpHandler.Cache[DestinationAddress];
        var srcMac = ArpHandler.Cache[SourceAddress];

        return new EthernetHeader
        {
            EtherType = EtherType.Ip,
            DestinationMac = dstMac,
            SourceMac = srcMac
        };
    }

    public IpHeader CreateReplyHeader(int length)
    {
        return new IpHeader((ushort) length, Protocol, DestinationAddress, SourceAddress);
    }
}

[StructLayout(LayoutKind.Sequential, Pack = 1)]
public struct PseudoIpHeader(uint sourceAddress, uint destinationAddress, IpProtocol protocol, ushort length)
{
    [NetworkEndianness]
    public uint _sourceAddress = sourceAddress;
    [NetworkEndianness]
    public uint _destinationAddress = destinationAddress;
    public byte _zeroes = 0;
    public IpProtocol _protocol = protocol;
    [NetworkEndianness]
    public ushort _length = length;
}
