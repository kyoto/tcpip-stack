// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

using System.Runtime.InteropServices;

namespace TcpIpUserspace.TunTap;

[Flags]
public enum InterfaceRequestFlags : short
{
    Tap = 0x0002,
    NoPacketInformation = 0x1000,
}

[StructLayout(LayoutKind.Explicit)]
public struct InterfaceRequest
{
    private const int NameSize = 16;
    public const int IoControlTunTap = 1074025674;
    
    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = NameSize)]
    [FieldOffset(0)]
    public string Name;

    [MarshalAs(UnmanagedType.I2)]
    [FieldOffset(NameSize)]
    public InterfaceRequestFlags Flags;
    
    [MarshalAs(UnmanagedType.I8)]
    [FieldOffset(NameSize)]
    private long _r1;
    
    [MarshalAs(UnmanagedType.I8)]
    [FieldOffset(NameSize + 8)]
    private long _r2;
    
    [MarshalAs(UnmanagedType.I8)]
    [FieldOffset(NameSize + 16)]
    private long _r3;
}
