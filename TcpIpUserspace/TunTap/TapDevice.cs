using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using Microsoft.Win32.SafeHandles;
using TcpIpUserspace.Util;
using static Tmds.Linux.LibC;

namespace TcpIpUserspace.TunTap;

public class TapDevice
{
    private readonly string _name;

    public readonly FileStream Stream;
    public readonly NetworkInterface Interface;

    private static TapDevice? _instance;

    public uint IpAddress
    {
        get
        {
            var addr = IPAddress.Parse("10.0.0.1");

            return BitConverter.ToUInt32(addr.GetAddressBytes());
        }
    }

    private TapDevice(ref string name)
    {
        var fd = OpenDevice(ref name);
        _name = new string(name);
        
        SetInterfaceUp();
        SetInterfaceRoute();
        SetInterfaceAddress();

        var handle = new SafeFileHandle(fd, true);
        Stream = new FileStream(handle, FileAccess.ReadWrite, 2048, false);
        
        Interface = NetworkInterface
            .GetAllNetworkInterfaces()
            .First(i => i.Name == _name);
    }

    public static uint DefaultAddress()
    {
        return ByteUtils.HostToNetworkOrder(BitConverter.ToUInt32(IPAddress.Parse("10.0.0.2").GetAddressBytes()));
    }

    public static TapDevice GetInstance()
    {
        if (_instance == null)
        {
            var name = "tap%d";
            _instance = new TapDevice(ref name);
        
            Console.Error.WriteLine($"Created TAP device {name}");
        }

        return _instance;
    }

    private unsafe int OpenDevice(ref string name)
    {
        int fd;
        fixed (byte *buffer = "/dev/net/tun"u8)
        {
            fd = open(buffer, O_RDWR);
        }

        if (fd < 0)
        {
            Console.Error.WriteLine("Cannot open TUN/TAP dev");
            Environment.Exit(1);
        }

        var ifr = new InterfaceRequest
        {
            Name = name,
            Flags = InterfaceRequestFlags.Tap | InterfaceRequestFlags.NoPacketInformation
        };

        var ifrBuffer = ByteUtils.StructToByteArray(ifr);

        fixed (void *buffer = ifrBuffer)
        {
            if (ioctl(fd, InterfaceRequest.IoControlTunTap, buffer) < 0)
            {
                byte[] buf = new byte[128];
                fixed (byte *ptr = buf)
                    strerror_r(errno, ptr, 128);

                string error = Encoding.ASCII.GetString(buf);
                
                close(fd);
                throw new IOException($"Could not ioctl tap: {error}");
            }
        }
        
        var result = ByteUtils.ByteArrayToStruct<InterfaceRequest>(ifrBuffer, out _);

        name = result.Name;

        return fd;
    }

    private void RunCommand(string command)
    {
        var info = new ProcessStartInfo
        {
            FileName = "/bin/bash",
            Arguments = $"-c \"{command}\"",
            RedirectStandardOutput = true,
            UseShellExecute = false,
            CreateNoWindow = true,
            ErrorDialog = false
        };
        
        var process = new Process
        {
            StartInfo = info
        };

        process.Start();
    }

    private void SetInterfaceUp()
    {
        RunCommand($"ip link set dev {_name} up");
    }

    private void SetInterfaceRoute()
    {
        RunCommand($"ip route add dev {_name} 10.0.0.0/24");
    }

    private void SetInterfaceAddress()
    {
        RunCommand($"ip address add dev {_name} local 10.0.0.1");
    }
}